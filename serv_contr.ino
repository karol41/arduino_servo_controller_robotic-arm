#include <Servo.h>
#include <Stepper.h>

// stepper
const int stepsPerRevolution = 2038;
// Stepper myStepper0(stepsPerRevolution, 1, 3, 2, 4);
Stepper myStepper0(stepsPerRevolution, 2, 4, 3, 5);
Stepper myStepper1(stepsPerRevolution, 6, 8, 7, 9);
Stepper myStepper2(stepsPerRevolution, 10, 12, 11, 13 );
// servo
Servo myServo1;
int servo_pin_1 = 6;
int servo_pin_read = 1;
const int stepperSpeed = 10;
void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600);
  myServo1.attach(A4);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  // pinMode(A4, INPUT_PULLUP);
//  pinMode(servo_pin_read, INPUT);
}

int servo_position = 90;
int x_axis_pos = 0;
int y_axis_pos = 0;
int z_rotation = 0;
int x_axis_pos_previous = 0;
int y_axis_pos_previous = 0;
int z_rotation_previous = 0;
bool switch_dir = true;

int previous = 0;
void loop() {

  x_axis_pos = analogRead(A0) / 10 * 10;
  y_axis_pos = analogRead(A1) / 10 * 10;
  z_rotation = analogRead(A2) / 10 * 10; 
  // https://www.arduino.cc/en/Tutorial/MotorKnob
  // int servo_val = map(x_axis_pos, 0, 1023, 0, 180);
//  Serial.println(x_axis_pos);
  //  Serial.println(servo_val);
//   Serial.println(z_rotation);

  //  myServo1.write(servo_val);
  myStepper0.setSpeed(stepperSpeed); // 6 rpm
  myStepper1.setSpeed(stepperSpeed); // 6 rpm
  myStepper2.setSpeed(stepperSpeed); // 6 rpm

    stepperMove(myStepper0, x_axis_pos);
    stepperMove(myStepper1, y_axis_pos);
    stepperMove(myStepper2, z_rotation);
   int check_pin = digitalRead(servo_pin_read);
    check_pin = digitalRead(A3);
    // check_pin =  map(analogRead(A3), 0, 1023, 0, 1); //get the pin from
    // analog
    // Serial.println(check_pin);
//    check_pin = map(digitalRead(A3), 0, 1023, 0, 1); // get the pin from analog

//    Serial.print("my pin");
//    Serial.println(check_pin);

int check_position = myServo1.read();
       Serial.print("Servo position");
       Serial.println(check_position);
       
    if (!check_pin == 0) {
      if (switch_dir == true) {
        servo_position -= 1;
      } else {
        servo_position += 1;
      }

//      if (servo_position > 180 || servo_position < 0) {
        if (servo_position > 160 || servo_position < 90) {
        switch_dir = !switch_dir;
      }
      myServo1.write(servo_position);

      //    Serial.println(servo_position);
  }
}

void stepperMove(Stepper my_stepp, int val) {

  int motorMove = map(val, 0, 1023, -stepperSpeed, stepperSpeed);

  int step_mult = 500;
  if (motorMove != 0) {
//    Serial.print("motor move");
//    Serial.println(motorMove);
    my_stepp.setSpeed(abs(motorMove));
    if (motorMove > 1 ) {
      my_stepp.step(stepsPerRevolution / step_mult);
    } else if (motorMove < -1 ) {
      my_stepp.step(-1 * stepsPerRevolution / step_mult);
    }
    delay(1);
  }
}

